---
date: 2024-03-18 12:12:35+02:00
description: Welcome page
lastmod: 2024-07-18 23:05:07.351995
tags:
- index
title: Welcome
---

to my homepage,

I'll write here all I know or find out.

At the moment there will be things about M\$ Windows, Arch Linux, bash-scripting, Python and some programs I'm using. Additionally you'll find a lot of old stuff I used to write in the past.

All hints presented here are tested by myself but I don't give a warranty of the functionality on each system.

For questions about these sites you can send me an e-mail `lord-weber-andrwe <at> andrwe <dot> org`

---

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/) and [Hugo](https://gohugo.io).

It uses the [`congo` theme](https://github.com/jpanther/congo)
