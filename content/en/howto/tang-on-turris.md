---
date: 2023-03-18T14:37:00+01:00
description: "Tang on Turris Omnia"
tags: ["tang", "turris", "security", "encryption"]
title: "Tang server on Turris Omnia"
---

{{< security >}}

{{< warranty >}}

I'm using Turris Omnia as router.

My server is storing its data on encrypted ZFS while the encryption key is a
random blob file stored on a cryptsetup volume.
The cryptsetup is decrypted automatically upon boot using Clevis & Tang approach.
As the router is always running I choose it as system running Tang.

The tang version provided by Turris is to old for latest Clevis setups.
That is why installing the package from repository does not work.
Due to that I decided to run Tang within LXC.

Here are the steps for that:

1. login to router via SSH
1. update templates and create container:

   ```bash
   lxc-create -n tang -t download -- --no-validate -a "$(uname -m)" -r latest -d ArchLinux
   ```

1. connect to LXC container: `lxc-attach tang`
1. install updates: `pacman -Syu`
1. install Tang: `pacman -Sy tang`
1. change hostname: `hostnamectl hostname tang`
1. disable unnecessary services:

   ```bash
   systemctl stop container-getty@{0..3}.service console-getty.service man-db.timer shadow.timer
   systemctl mask container-getty@{0..3}.service console-getty.service man-db.timer shadow.timer
   ```

1. configure tangd port as default 80 is already used by TurrisOS:

   ```bash
   mkdir /etc/systemd/system/tangd.socket.d
   cat > /etc/systemd/system/tangd.socket.d/socket.conf <<EOF
   [Socket]
   ListenStream=8888
   Accept=true
   EOF
   ```

1. start tangd

   ```bash
   systemctl enable tangd.socket --now
   ```

1. force key generation

   ```bash
   curl localhost:8888/adv
   ```

1. backup the files in `/var/db/tang/` to a really secure place (e.g. password safe) as you will not be able to decrypt any data without them
1. leave container via Ctrl+d or `exit`-command
1. get IP address of container: `lxc-ls -f`
1. when DHCP clients may not push hostnames, configure your DNS to point to this IP address
1. configure your clients to connect to the DNS

:tada: Congratulations you are running Tang on your Turris router. :tada:
