---
date: 2016-07-12 09:19:20+02:00
description: Testing Saltstack
lastmod: 2024-07-18 23:10:57.434715
tags:
- saltstack
title: salt
---

{{< incomplete >}}

[SaltStack](http://saltstack.com) is a configuration management and remote execution tool used to manage servers.

Here is a collection of steps I used to create my own saltstack master and configure minions.

## Master with MySQL pillars

The saltstack master is the control server all minions connect to. It specifies the packages and configuration files a minion should install and commands the minion has to run.

I'm running the master using the [mysql backend](https://docs.saltstack.com/en/latest/ref/pillar/all/salt.pillar.mysql.html) for pillars.

1. install salt-raet, for ArchLinux:

   ```bash
   pacman -Sy salt-zmq mysql-python
   ```

1. create file root dir:

   ```bash
   mkdir -p salt/files
   ```

1. configure master:

   ```yaml
   hash_type: sha512
   file_roots:
     base:
       - /srv/salt/files/
   ```

1. mysql:

   1. set timezone:

      ```bash
      mysql_tzinfo_to_sql /usr/share/zoneinfo/ | mysql -uroot mysql
      ```

   1. create databases:

      ```sql
      CREATE USER 'salt'@'localhost' IDENTIFIED BY 'CHANGEME';
      CREATE USER 'cmdb'@'localhost' IDENTIFIED BY 'CHANGEME';
      CREATE DATABASE cmdb;
      GRANT SELECT ON `cmdb`.* TO 'salt'@'localhost';
      GRANT ALL PRIVILEGES ON `cmdb`.* TO 'cmdb'@'localhost';
      USE cmdb;
      CREATE TABLE companies (id INT AUTO_INCREMENT PRIMARY KEY, company VARCHAR(255) UNIQUE NOT NULL);
      CREATE TABLE companies_projects (id INT AUTO_INCREMENT PRIMARY KEY, companyid INT NOT NULL, project VARCHAR(255) NOT NULL, CONSTRAINT `company` FOREIGN KEY (companyid) REFERENCES companies (id) ON DELETE CASCADE ON UPDATE RESTRICT);
      CREATE TABLE companies_projects_role (id INT AUTO_INCREMENT PRIMARY KEY, projectid INT NOT NULL, role VARCHAR(255) NOT NULL, CONSTRAINT `company_project` FOREIGN KEY (projectid) REFERENCES companies_projects (id) ON DELETE CASCADE ON UPDATE RESTRICT);
      CREATE TABLE pillars (id INT AUTO_INCREMENT PRIMARY KEY, roleid INT NOT NULL, pil1 VARCHAR(255) NOT NULL, pil2 VARCHAR(255) NOT NULL, pil3 VARCHAR(255) NOT NULL, pil4 VARCHAR(255) NOT NULL, pil5 VARCHAR(255) NOT NULL, pil6 VARCHAR(255) NOT NULL, pil7 VARCHAR(255) NOT NULL, CONSTRAINT `company_project_role` FOREIGN KEY (roleid) REFERENCES companies_projects_role (id) ON DELETE CASCADE ON UPDATE RESTRICT);
      ```
