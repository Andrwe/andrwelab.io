---
date: 2011-10-02 17:55:27+02:00
description: Archlinux Repository
lastmod: 2024-07-18 23:10:57.435552
tags:
- linux
- repository
- deprecated
title: arch
---

{{< deprecated >}}

Since about a year I'm using [Arch Linux](http://archlinux.org).  
I like this distribution because all I want to do myself I can do myself and all other stuff is done by the system.

## Repository

I've started to build a repository for programs I'm using from [AUR](http://aur.archlinux.org) because I'm using 3 computers with archlinux and I've installed some programs on all of them.  
So I'm compiling these programs on one system and provide them to the others through my repository.

If you want to use my repository for your Arch Linux add this with the correct architecture to your /etc/pacman.conf:

```ini
[andrwe]
Server = http://andrwe.org/repo/<architecture>
```

Although the steps to build packages and a repository are very easy I've written a script to automate it for me.  
[makerepopkg](/makerepopkg)

And I've also written a script in python to build the file "repository.files.tar.gz" which is needed by pkgtools.  
[createfiles.py](/createfiles)

This script can be used to check the up-to-date status of an own build repository.  
[upcheck.sh](/upcheck)
