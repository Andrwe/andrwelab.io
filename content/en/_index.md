---
date: 2024-03-18 12:12:35+02:00
description: Main page for EN
lastmod: 2024-07-18 23:05:07.352362
tags:
- index
title: Main
---

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
and [Hugo](https://gohugo.io).
It uses the [`congo` theme](https://github.com/jpanther/congo)
