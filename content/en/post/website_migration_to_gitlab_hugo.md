---
date: 2024-07-20 13:47:25.234703
description: Website Migration To Gitlab & Hugo
tags:
- documentation
- Hugo
- Gitlab
- Pre-Commit
title: Website Migration To Gitlab & Hugo
---

To reduce maintainability of my website I'm migrating it from an old dokuwiki installation to [Hugo](https://gohugo.io/) generated static HTML on [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/).

## Migration

For the migration I used the method of [Natenom](https://natenom.de/2022/03/umzug-dokuwiki-hugo-1-vorarbeiten/) with a slightly modified script for prepending the Frontmatter to add the date based on the file creation time:

```bash
#!/bin/bash

set -e
set -o pipefail

filepath="$1" #path/to/filename.md
file_name="$(basename "$filepath")" #filename.md

md='.txt.md'
title="${file_name%"$md"}" #filname
date="$(date -d @"$(stat --format '%Y' "pages/${filepath%%.md}")" +'%Y-%m-%dT%H:%M:%S%:z')"


TEMPLATE="---
title: $title
date: ${date}
---"

# Remove first line of file (with # Title)
sed -i '1d' "${filepath}"

(
    echo "$TEMPLATE"
    cat "$filepath"
) > temp
mv temp "${filepath%"$md"}.md"
rm "${filepath}"
```

Afterwards I review every page to fix markdown code and add further Frontmatter.
This will take some time so feel free to come back here later.

As I tend to forget the syntax of required Frontmatter I create [hooks](https://gitlab.com/Andrwe/alw-pre-commit-hooks) for [pre-commit](https://pre-commit.com/) that add the Frontmatter if fully missing and informs about missing required keys.
