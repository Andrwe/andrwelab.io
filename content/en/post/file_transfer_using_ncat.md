---
date: 2012-08-30 12:12:35+02:00
description: This setup provides a fast way to copy files between different computers.
lastmod: 2024-07-18 02:39:45.501739
tags:
- linux
- ncat
title: File Transfer Using Ncat
---

Hi,

today [seiichiro0185](http://seiichiro0185.org) and I have found a fast and unusual way to copy files.

{{< security >}}
{{< warranty >}}

Dependencies:

- [ncat](http://nmap.org/ncat/) (mostly provided by nmap)

## Setup

### file recieving system

1. copy this script on your system which should recieve the files:

   ```bash
   #!/bin/bash

   # directory all files and folders will be copied into
   basedir="/<any base directory>/"

   read filename
   echo "$filename" >&2
   if [[ "${filename}" =~ / ]]
   then
     [ -d "${basedir}/${filename%%/*}" ] || mkdir -p "${basedir}/${filename%%/*}"
   fi
   cat - > "${basedir}/${filename}"
   ```

2. run the following command:

   ```bash
   ncat -lkvnp 3334 --recv-only -e /path/to/your/recv-file.sh
   ```

Now the server is running and listening on port 3334. All files and directories are created with permissions of the running ncat.

### file sending system

1. copy the following script on the system you to send files from:

   ```bash
   #!/bin/bash

   host="$1"
   port="$2"

   [[ ${port} =~ ^[0-9][0-9]*$ ]] || { echo "The given port $port (2. argument) is not valid." >&2 && exit 1 ; }
   shift 2

   IFS=","
   for f in $@
   do
     { echo "${f}" && cat "${f}" ; } | ncat -i1 $host $port
   done
   ```

1. use the script like this:

   ```bash
   bash send-file.sh <host> <port> <file1>,<file2>,..,<fileN>
   ```

If you give the directory in your filelist they are also created on the server side.
