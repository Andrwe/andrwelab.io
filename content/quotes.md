---
date: 2011-11-19 13:17:07+01:00
description: Some Quotes
lastmod: 2024-07-18 23:10:57.435008
tags:
- fun
title: Quotes
---

Here are some quotes I like.

---

Taylor's Laws of Programming:

> "Never write it in 'C' if you can do it in 'awk';
>
> Never do it in 'awk' if 'sed' can handle it;
>
> Never use 'sed' when 'tr' can do the job;
>
> Never invoke 'tr' when 'cat' is sufficient;
>
> Avoid using 'cat' whenever possible."

---

Alan Kay:

> Most software today is very much like an Egyptian pyramid with
>
> millions of bricks piled on top of each other, with no structural
>
> integrity, but just done by brute force and thousands of slaves.

---

<https://notomorrow.de/welcome>:

> the redundant department of redundant redundancy admits:
> that you first have to understand recursion to understand recursion!

---

Vlad Dolezal:

(see on [archive.org](https://web.archive.org/web/20090129210800/http://blog.anamazingmind.com/2008/03/real-reason-we-use-linux.html))

> The REAL reason we use Linux
>
> We tell people we use Linux because it's secure. Or because it's free, because it's customizable, because it's free (the other meaning), because it has excellent community support...
>
> But all of that is just marketing bullshit. We tell that to non-Linuxers because they wouldn't understand the real reason. And when we say those false reasons enough, we might even start to believe them ourselves.
>
> But deep underneath, the real reason remains.
>
> We use Linux because it's fun!
>
> It's fun to tinker with your system. It's fun to change all the settings, break the system, then have to go to recovery mode to repair it. It's fun to have over a hundred distros to choose from. It's fun to use the command line.
>
> Let me say that again. It's fun to use the command line.
>
> No wonder non-Linuxers wouldn't understand.
>
> The point with us Linux fans is - we use Linux for its own sake. Sure, we like to get work done. Sure, we like to be secure from viruses. Sure, we like to save money. But those are only the side-effects. What we really like is playing with the system, poking around, and discovering completely pointless yet fascinating facts about the OS.
>
> There are three main reasons Linux is so much fun:
>
> 1. Linux gives you complete control
>
>    Ever tried stopping a process in Windows and the OS wouldn't let you? Ever tried deleting a file - and you couldn't? Even though you had admin rights?
>
>    Linux lets you do anything. That's the great benefit of usually logging in as user. If you login as the root, the OS assumes you know what you're doing. Once you become root, everything is allowed.
>
> 2. Linux isn't widely used
>
>    This is a paradox. We often complain Linux isn't more widely used. But that's one of the reasons we use it. It gives us a feeling of being a special clique. Like we're better than "those ignorant masses".
>
>    If Linux becomes widely used, we'll probably switch to something else. Or at least develop an obscure distro that only we will use. Because, let's face it, we want to feel special.
>
> 3. Linux is free (as-in-speech)
>
>    We can get the source code for all our applications. If we want to know how a certain part of the OS works, we can. This lets us tweak and play with our systems. And we absolutely loo-o-o-ve tweaking our system.
>
> Of course we can't tell non-Linuxers we use Linux because it's fun - they'd stick us into a mental asylum quicker than you can say "antidisestablishmentarianism". So we'll keep telling them the false yet plausible reasons for using Linux. But deep inside, we'll know the real reason we use Linux.
>
> And maybe, just maybe, next time someone asks me why I use Linux, I'll flash a huge smile and answer: "Because using Linux is FUN!"

---

Virak @ <https://linux.slashdot.org/comments.pl?sid=1400747&cid=29716371>:

> From the makers of the widely-acclaimed, award-winning hit Unix, comes Linux, an exciting game of patience and frustration!
>
> Risk your life to perform the sacred ritual of Installation to gain entry to the land of Linux, with the Dark Lord's minions Grub and Fdisk trying their hardest to stop you!
> Explore the fearsome depths of the labyrinthine cursed dungeon /etc in an attempt to find the ancient lost artifact, A Fucking Working Configuration!
> Engage in challenging battle with dozens of the Dark Lord's vile Sound Systems to free the people of the land of Linux from their oppressive tyranny and bring the joy of music to them!
> Uncover the true name of Linux itself through harrowing inane ideological debate, and use the vast trolling power of this to cause a major rift in the land over a fucking name!
> Face off against the Dark Lord Xorg himself in exciting one-on-one battle, and pry the holy twin swords of Multi-Monitor Support and Working 3D Acceleration from his cold, dead hands and bring peace to Linux at last--if you can!
>
> Linux promises upwards of 60 hours of unique and difficult gameplay, each moment full of exciting new threats and challenges in your attempt to free the land of Linux from the Dark Lord and his underlings at last and bring usability to all!
>
> (Warning: Linux should not be played by people with photosensitive epilepsy, pregnant woman, smokers, children shorter than this tall, BSD zealots, and anyone who doesn't actually want to fight Xorg and just wants their fucking computer to fucking work already)
