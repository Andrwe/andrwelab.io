---
date: 2012-01-06 21:37:43+01:00
description: Collection of Links
lastmod: 2024-07-18 23:10:57.434312
tags:
- fun
- support
title: Links
---

Here are some maybe useful links.

## Friends

[Seiichiros HP](http://seiichiro0185.org/) with useful stuff about GNU/Linux, bash, Nokia Nxx0 and more.  
[Eanderalx' HP](http://eanderalx.org/) with useful stuff about GNU/Linux, bash, perl and more.  

## GNU/Linux

### Arch Linux

[Arch Linux Mainpage](http://archlinux.org)  
[Arch Linux Wiki](http://wiki.archlinux.org)  
[Arch Linux Forum](https://bbs.archlinux.org)  

### Guides

* [Linux Beginner Guide](http://www.ee.surrey.ac.uk/Teaching/Unix/)  
* [Advanced Bash-Scripting Guide](http://tldp.org/LDP/abs/html/)  
* [Sed - An Introduction and Tutorial](http://www.grymoire.com/Unix/Sed.html)  
* [LaTeX-Befehlsreferenz: Übersicht](http://www.weinelt.de/latex/index.html)  

## Support

If you want to support me you can:

* create an account on Netcup with this referral link: <https://www.netcup.de/?ref=98583>
